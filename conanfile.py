from conans import ConanFile, CMake, tools
import shutil, os

class CRC32CConan(ConanFile):
    name             = "crc32c"
    version          = "1.1.1"
    license          = "MIT"
    url              = "https://bitbucket.org/toge/conan-crc32c/"
    homepage         = "https://github.com/google/crc32c"
    settings         = "os", "compiler", "build_type", "arch"
    description      = "A Template Engine for Modern C++"
    generators       = "cmake"

    def source(self):
        tools.get('https://github.com/google/crc32c/archive/{}.zip'.format(self.version))
        shutil.move("crc32c-{}".format(self.version), "crc32c")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CRC32C_BUILD_TESTS"]      = False
        cmake.definitions["CRC32C_BUILD_BENCHMARKS"] = False
        cmake.definitions["CRC32C_USE_GLOG"]         = False
        cmake.configure(source_folder="crc32c")
        cmake.build()

    def package(self):
        self.copy("*.h", src="crc32c/include", dst="include", keep_path=True)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["crc32c"]

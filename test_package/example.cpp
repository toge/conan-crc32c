#include <cstdlib>
#include <iostream>

#include "crc32c/crc32c.h"

int main(int argc, const char** argv) {
    (void)argc, (void)argv;

    crc32c::Crc32c("abcdefg", 7);

    return 0;
}
